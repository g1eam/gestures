var VanillaRunOnDomReady = function() {
    function Recognizer() {
}

Recognizer.prototype.learn = function(gesture, name) {
};

Recognizer.prototype.recognize = function(gesture) {
  var name = '';

  //gesture = this.filter(gesture);

  return name;
};

Recognizer.prototype.filter = function(gesture) {
  var len = gesture.length;
  var indicies = [0];

  // precalculated distances between pairs of points
  var distances = [0]; // [0, d1, d2, ...]
  for (var i = 1; i < len; i++) {
    var dx = gesture[i][0] - gesture[i-1][0];
    var dy = gesture[i][1] - gesture[i-1][1];
    var dd = Math.sqrt(dx * dx + dy * dy);
    distances.push(dd);
  }

  var i1 = 0;
  var segLength = distances[i1 + 1];

  for (var i2 = 2; i2 < len; i2++) {
    var p1 = gesture[i1];
    var p2 = gesture[i2];
    var dLength = distances[i2];

    // equations are:
    // x(d) = kx * d + bx
    // y(d) = ky * d + by
    var kx = (p2[0] - p1[0]) / (segLength + dLength);
    var bx = p1[0];
    var ky = (p2[1] - p1[1]) / (segLength + dLength);
    var by = p1[1];

    var ex = 0;
    var ey = 0;
    var eLimit = 0.025 * Math.pow(segLength + dLength, 2);

    // iterate all points between i1 and i2 (non-inclusive)
    var d = 0;
    for (var j = i1 + 1; j < i2; j++) {
      d += distances[j];
      ex += Math.pow((kx * d + bx) - gesture[j][0], 2);
      ey += Math.pow((ky * d + by) - gesture[j][1], 2);
    }

    if (ex > eLimit || ey > eLimit) {
      i1 = i2 - 1;
      segLength = dLength;
      indicies.push(i1);
    } else {
      segLength += dLength;
    }
  }

  indicies.push(gesture.length - 1);

  var newGesture = [];
  var len = indicies.length;
  for (var i = 0; i < len; i++) {
    newGesture.push(gesture[indicies[i]]);
  }
  return newGesture;
};

Recognizer.prototype.LAConversion = function(points) {
    var LAPoints = [];
    for(var i = 0; i < points.length - 1; i++) {
        var p1 = points[i];
        var p2 = points[i + 1];
        var dx = p2[0] - p1[0];
        var dy = p2[1] - p1[1];
        var l = Math.sqrt(dx * dx + dy * dy);
        var a = Math.atan2(-dy, dx) * toDegrees;
        LAPoints.push([l, a]);
    }
    return LAPoints;
}

function Logger(){};
Logger.prototype.points = function(pArray) {
    for(var i = 0; i < pArray.length; i++) {
        console.log(i, pArray[i][0].toFixed(2), pArray[i][1].toFixed(2));
    }
}

function init(me) {
    console.log('inited');
    var allPoints = undefined;
    var impPoints = undefined;
    var recognizer = new Recognizer();
    var logger = new Logger();

    var sw = canvasWidth, sh = canvasHeight; // size of the canvas
    me.size(sw, sh);

    var ox = sw / 2, oy = sh / 2; // offsets of middle point
    var startPoint = undefined;
    var lastPoint = undefined;
    var distance = 0.0;
    
    function mousePressed() {
        if (allPoints != undefined) allPoints = undefined;
        if (impPoints != undefined) impPoints = undefined;
        var x = me.mouseX - ox, y = me.mouseY - oy;
        startPoint = [x, y];
        lastPoint = [x, y];
    }
    
    function mouseDragged() {
        var x = me.mouseX - ox, y = me.mouseY - oy;
        
        if (allPoints === undefined) {
            allPoints = [startPoint.slice(), [x, y]];
        } else {
            allPoints.push([x, y]);
        }
        lastPoint = [x, y];
    }
    
    function mouseReleased() {
        distance = 0;
        lastPoint = undefined;
        console.log("points: \n")
        logger.points(impPoints);
        console.log("LApoints: \n")
        logger.points(recognizer.LAConversion(impPoints));
    }

    function draw() {
        me.background(colors['white']);
        
        me.noFill();
        me.stroke(colors['red']);
        me.strokeWeight(1);
        me.rect(ox - canvasWidth / 2 + border, oy - canvasHeight / 2 + border, canvasWidth - 2 * border, canvasHeight - 2 * border);
        
        drawPath();
    }

    function drawPath() {
        if (allPoints !== undefined && allPoints.length > 2) {
            me.strokeWeight(3);
            me.stroke(colors['green']);
            for (var i = 0; i < allPoints.length; i++) {
                me.point(ox + allPoints[i][0], oy + allPoints[i][1]);
            }
        }
        if (allPoints !== undefined) {
            impPoints = recognizer.filter(allPoints);
        }
        if (impPoints !== undefined) {
            me.stroke(colors['red']);
            me.strokeWeight(5);
            for (var i = 0; i < impPoints.length; i++) {
                me.point(ox + impPoints[i][0], oy + impPoints[i][1]);
            }
        }
    }

    me.draw = draw;
    me.mousePressed = mousePressed;
    me.mouseReleased = mouseReleased;
    me.mouseDragged = mouseDragged;

}

// == Data ==
  // canvas-size:
    var canvasWidth = 800;
    var canvasHeight = 500;
    var border = -2;

var colors = {
    // 0xAARRGGBB
    'white': 0xFFFFFFFF,
    'red': 0xFFFF3333,
    'green': 0xFF33AA33
};
var toDegrees = 180/Math.PI;

// == Initialization ==
var canvas = document.getElementById("canva");
var processing = new Processing(canvas, init);

}

var alreadyrunflag = 0;

if (document.addEventListener)
    document.addEventListener("DOMContentLoaded", function(){
        alreadyrunflag=1; 
        VanillaRunOnDomReady();
    }, false);
else if (document.all && !window.opera) {
    document.write('<script type="text/javascript" id="contentloadtag" defer="defer" src="javascript:void(0)"><\/script>');
    var contentloadtag = document.getElementById("contentloadtag")
    contentloadtag.onreadystatechange=function(){
        if (this.readyState=="complete"){
            alreadyrunflag=1;
            VanillaRunOnDomReady();
        }
    }
}

window.onload = function() {
  setTimeout("if (!alreadyrunflag){VanillaRunOnDomReady}", 0);
} 